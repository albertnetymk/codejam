import Text.Printf (printf)
import Data.Digits (digits, unDigits)
import Data.List (intercalate, nub, delete)
import Data.Numbers.Primes (primes)

solve :: Int -> Int -> [[String]]
solve n j =
  map snd
    $ take j
    $ filter fst
    $ map proof
    $ map (\l -> (1:(replicate (n - 1 - (length l)) 0) ++ l))
    $ map (digits 2)
    $ [1, 3..2^(n-1)-1]

prob_factor :: Integer -> Int
prob_factor x = prob_factor' primes x
  where
    prob_factor' (p:rest) x
      | p*p >= x || p > 1000 = 0
      | x `mod` p == 0 = fromIntegral p
      | otherwise = prob_factor' rest x
    prob_factor' _ _ = error "empty primes"

prob_is_prime :: Integer -> Bool
prob_is_prime x = prob_factor x == 0

proof :: [Integer] -> (Bool, [String])
proof bits =
  let
    tuples = map each [2..10]
    fsts = map fst tuples
    number_str = intercalate "" $ map show $ bits
    is_legit = and fsts
    factors = map (show . snd) tuples
  in (is_legit, (number_str:factors))
  where
    each base =
      let number = unDigits base bits
      in (not $ prob_is_prime number, prob_factor number)

-- for p = [2:5]
--     nchoosek(16 - p - 2 , 16 - p*2)
-- end
-- ans =  1
-- ans =  11
-- ans =  45
-- ans =  84

-- for p = [2:5]
--     nchoosek(32 - p - 2 , 32 - p*2)
-- end
-- ans =  1
-- ans =  27
-- ans =  325
-- ans =  2300

mine1 :: Int -> Int -> [[String]]
mine1 n j =
  let
    sum = n - 10
    tuples = [(a,b,c,sum-a-b-c) | a<-[0..sum], b<-[0..sum-a], c<-[0..sum-a-b]]
  in take j $ map ((:[factors]) . show_list . each) tuples
  where
    factors = "3 2 5 2 7 2 3 2 11"
    show_list = intercalate "" . map show
    each (a,b,c,d) =
      [1,1] ++ replicate a 0 ++ [1,1] ++ replicate b 0 ++
      [1,1] ++ replicate c 0 ++ [1,1] ++ replicate d 0 ++ [1,1]

eq_perms [] = [[]]
eq_perms xs = [x:xt | x <- nub xs, xt <- eq_perms $ delete x xs]

merge [] ys = ys
merge (x:xs) ys = x:merge ys xs

pad ones length = replicate ones 1 ++ replicate (length - ones) 0

-- https://teresamccullough.wordpress.com/2011/05/25/divisibility-rules-for-numbers-in-bases-other-than-ten/
-- (nchoosek((16 - 2)/2, 2))^2 => ans =  441
-- (nchoosek((32 - 2)/2, 2))^2 => ans =  11025

mine2 :: Int -> Int -> [[String]]
mine2 n j =
  let
    ones = quot (total-2) 2
    odd = pad (ones + n `mod` 2) high_half
    even = pad (ones - n `mod` 2) low_half
  in combine odd even
  where
    combine odd even =
      take j $ map each $ [merge o e | o <- eq_perms odd, e <- eq_perms even]
    total = 6
    each list = intercalate "" (map show $ [1] ++ list ++ [1]) : [factors]
    factors = "3 2 3 2 7 2 3 2 3"
    high_half = quot (n + 1 - 2) 2
    low_half = quot (n - 2) 2

test_case :: Int -> Int -> IO ()
test_case index total
  | index == (total+1) = return ()
  | otherwise = do
    [n, j] <- fmap (map read . words) getLine
    printf "Case #%d:\n" index
    -- mapM_ (putStrLn . intercalate " ") $ solve n j
    -- mapM_ (putStrLn . intercalate " ") $ mine1 n j
    mapM_ (putStrLn . intercalate " ") $ mine2 n j
    test_case (index+1) total

main = do
  -- mapM_ (putStrLn . intercalate " ") $ solve 16 1000
  -- mapM_ (putStrLn . intercalate " ") $ mine1 16 1000
  -- mapM_ (putStrLn . intercalate " ") $ mine2 16 1000
  total <- fmap read getLine
  test_case 1 total
