## count_sheep

The most challenging part is to prove (or believe) that no numbers other than zero could result in insomnia. The intuition is that the first digit can
only grow by 1 at most, then it's guaranteed that we would see [1..9] ++ [0] in this order.

## pancake

The trick is to define "group height", and analyze how flipping would change it. As a result, it's possible to calculate the number of steps without
actually doing the steps.

## coin

This deserves its own [post](http://albertnetymk.github.io/2016/06/26/jamcoin/).

## fractiles

The key idea is that one tile carries not only its parent's info, but also the tile that shares the same local index in previous generation.
