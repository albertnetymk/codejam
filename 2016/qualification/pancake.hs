import Text.Printf (printf)
import Data.Text (pack, breakOnAll)

flip_side :: Char -> Char
flip_side '-' = '+'
flip_side '+' = '-'
flip_side _ = error "no match"

solve :: [Char] -> Int
solve list = solve' list 0
  where
    solve' list count
      | not $ '-' `elem` list = count
      | otherwise =
        case list of
          x : rest ->
            let (head, tail) = break (/=x) list
            in solve' (map flip_side head ++ tail) (count+1)
          [] -> error $ "empty list"

count_sub_str str sub = length $ breakOnAll (pack sub) (pack str)

count_directly :: [Char] -> Int
count_directly list =
  let
    extra = if last list == '-' then 1 else 0
    height = 1 + count_sub_str list "+-" + count_sub_str list "-+"
  in height - 1 + extra

test_case :: Int -> Int -> IO ()
test_case index n
  | index == (n+1) = return ()
  | otherwise = do
    list <- getLine
    -- printf "Case #%d: %d\n" index $ solve list
    printf "Case #%d: %d\n" index $ count_directly list
    test_case (index+1) n

main = do
  n <- fmap read getLine
  test_case 1 n
