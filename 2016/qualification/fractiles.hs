import Text.Printf (printf)
import Data.List (intercalate)

solve :: Integer -> Integer -> Integer -> String
solve k c s =
  case indices k c s of
    [] -> "IMPOSSIBLE"
    list -> intercalate " " $ map show list

indices :: Integer -> Integer -> Integer -> [Integer]
indices k c s
  | c*s < k = []
  | otherwise = [n_level i i c | i <- [1,1+c..k]]
  where
    n_level p _ 1 = p
    n_level p i n
      | i /= k = n_level ((p-1)*k + i+1) (i+1) (n-1)
      | otherwise = n_level ((p-1)*k + k) k (n-1)

test_case :: Int -> Int -> IO ()
test_case index n
  | index == (n+1) = return ()
  | otherwise = do
    [k, c, s] <- fmap (map read . words) getLine
    printf "Case #%d: %s\n" index $ solve k c s
    test_case (index+1) n

main = do
  n <- fmap read getLine
  test_case 1 n
