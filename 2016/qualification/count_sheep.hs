import Text.Printf (printf)
import Data.Digits (digits)
import qualified Data.IntSet as IntSet

solve :: Int -> String
solve 0 = "INSOMNIA"
solve seed = solve' seed seed IntSet.empty
  where
    solve' :: Int -> Int -> IntSet.IntSet -> String
    solve' seed cur set
      | IntSet.size set == 10 = show $ cur - seed
      | otherwise =
        let new_set = IntSet.union set $ IntSet.fromList $ digits 10 cur
        in solve' seed (cur+seed) new_set

test_case :: Int -> Int -> IO ()
test_case index n
  | index == (n+1) = return ()
  | otherwise = do
    seed <- fmap read getLine
    printf "Case #%d: %s\n" index $ solve seed
    test_case (index+1) n

main = do
  n <- fmap read getLine
  test_case 1 n
